# -*- coding: utf-8

'''
Created on 28/08/2012

@author: Daniel
'''

from __future__ import unicode_literals
from datetime import datetime
from google.appengine.api import mail
from google.appengine.ext import ndb
from reuniao.model import Reuniao, Convidado
from usuario.model import User
from zen.ce import cengine

class home(object):    
    def index(self):
        u = User.query().get()
        query = Reuniao.query(Reuniao.usr==u.key)
        reunioes = query.fetch(100)
        values={"reunioes":reunioes,
                "usuario":u,
                "cont":query.count()}
        self.write_template("reunioes.html",values)                    

class acompanhamento():
    def index(self, idR):
        idreuniao=int(idR) 
        keyReuniao = ndb.Key(Reuniao, idreuniao)       
        query = Convidado.query(Convidado.reuniao == keyReuniao)        
        conv = query.fetch(100)                               
        reuniao = keyReuniao.get()
        values={"conv":conv,
                "reuniao":reuniao}        
        self.write_template("acompanhamento.html", values) 
        
class convidados():
    def index(self, idR):
        idreuniao=int(idR) 
        keyReuniao = ndb.Key(Reuniao, idreuniao)       
        query = Convidado.query(Convidado.reuniao == keyReuniao)
        conv = query.fetch(100)        
        salvar_url=cengine.to_path(convidados.salvar)                        
        values={"salvar_url":salvar_url,
                "idR":idreuniao,
                "conv":conv}        
        self.write_template("convidados.html",values)
    
    def salvar(self, idR):
        idreuniao=int(idR)
        keyReuniao = ndb.Key(Reuniao, idreuniao)
        c=Convidado(
                    reuniao=keyReuniao,
                    nome=self.get("nome"),
                    email=self.get("email"),
                    situacao=0                    
                    )
        c.put()        
        self.redirect("/reuniao/convidados/"+str(idreuniao))
        
    def enviarconvite(self,idC):
        idConvidado=int(idC)        
        keyConvidado = ndb.Key(Convidado, idConvidado)
        c = keyConvidado.get()
        keyReuniao = ndb.Key(Reuniao, c.reuniao.get().key.id())
        r = keyReuniao.get()
        mail.send_mail(sender=User.current_user().name+" <"+User.current_user().email+">",
              to=c.nome+" <"+c.email+">",
              subject="Convite de Reunião: "+r.titulo,
              body='''
Olá '''+c.nome+'''

Você foi convidado para uma reunião para falar dos seguintes assuntos:
'''+r.assunto+'''

Clique no link abaixo e dê sua resposta:
http://minhareuniao.appspot.com/resposta/'''+keyConvidado.urlsafe()+'''

Equipe
MinhaReuniao.com.br  
''')      
        self.redirect("/reuniao/convidados/"+str(c.reuniao.get().key.id()))  
        
    def excluir(self,idC):
        idConvidado=int(idC)
        keyConvidado = ndb.Key(Convidado, idConvidado)
        c = keyConvidado.get()
        c.key.delete()
        self.redirect("/reuniao/convidados/"+str(c.reuniao.get().key.id()))
        
        
class editar():
    def index(self,idR):
        idreuniao=int(idR)
        key = ndb.Key(Reuniao, idreuniao)
        usr = key.get()
        atualizar_url=cengine.to_path(atualizar)
        values={"u":usr,
                "atualizar_url":atualizar_url}
        self.write_template("editar.html",values)
        
class atualizar():
    def index(self,idR):
        idreuniao=int(idR)
        key = ndb.Key(Reuniao, idreuniao)
        u = User.query().get()
        horario = self.get("horario")
        h = horario.split(":")
        data = self.get("quando")
        d = data.split("/")
        r=Reuniao(
                  key=key,
                  tditulo=self.get("titulo"),
                  onde=self.get("onde"),
                  assunto=self.get("assunto"),
                  datahora=datetime(int(d[2]), int(d[1]), int(d[0]), int(h[0]), int(h[1]), 0),
                  usr=u.key
                  )
        r.put()
        self.redirect("/reuniao")        
        
class busca():
    def index(self):
        busca = self.get("busca")
        query = Reuniao.query(Reuniao.titulo >= busca)
        reunioes = query.fetch(100)                
        values={"reunioes":reunioes,
                "cont":query.count()}
        self.write_template("busca.html",values)
        
class buscacompleta():
    def index(self):
        u = User.query().get()
        busca = self.get("busca")
        query = Reuniao.query(Reuniao.usr==u.key)
        reunioes = query.fetch(100)
        for r in reunioes:
            print(r.titulo)   
        values={"reunioes":reunioes,
                "cont":query.count()}
        self.write_template("busca.html",values)        
        
class nova():
    def index(self):
        url=cengine.to_path(nova.salvar)
        values={"salvar_url":url}
        self.write_template("nova.html",values) 

    def salvar(self):
        u = User.query().get()
        horario = self.get("horario")
        h = horario.split(":")
        data = self.get("quando")
        d = data.split("/")
        r=Reuniao(
                  titulo=self.get("titulo"),
                  onde=self.get("onde"),
                  assunto=self.get("assunto"),
                  datahora=datetime(int(d[2]), int(d[1]), int(d[0]), int(h[0]), int(h[1]), 0),
                  usr=u.key
                  )
        r.put()
        self.redirect("/reuniao")
        
class deletar():
    def index(self,idR):
        idreuniao=int(idR)
        key = ndb.Key(Reuniao, idreuniao)
        key.delete()
        self.redirect("/reuniao")