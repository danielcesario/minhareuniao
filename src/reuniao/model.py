'''
Created on 10/09/2012

@author: Daniel
'''

from google.appengine.ext import ndb
from usuario.model import User

#===============================================================================
# class Reuniao(object):
#    def __init__(self,titulo,nome,email,data,hora,onde,assunto):
#        self.titulo=titulo
#        self.nome=nome
#        self.email=email
#        self.data=data
#        self.hora=hora        
#        self.onde=onde
#        self.assunto=assunto
#===============================================================================
        
class Reuniao(ndb.Model):
    titulo      =   ndb.StringProperty(required=True)    
    usr         =   ndb.KeyProperty(User, required=True)
    datahora    =   ndb.DateTimeProperty(required=True)
    onde        =   ndb.StringProperty(required=True)
    assunto     =   ndb.StringProperty(required=True)  
    
class Convidado(ndb.Model):    
    reuniao     =   ndb.KeyProperty(Reuniao, required=True)    
    nome        =   ndb.StringProperty(required=True)
    email       =   ndb.StringProperty(required=True)
    situacao    =   ndb.IntegerProperty(required=True)
    resposta    =   ndb.StringProperty(required=False)

#Situacao: 0 Aguardando / 1 Talvez / 2 Cofirmado / 3 Nao vai 