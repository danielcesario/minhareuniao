'''
Created on 03/11/2012

@author: Daniel
'''
from google.appengine.ext import ndb
from reuniao.model import Reuniao, Convidado

class home():
    def index(self,idC):        
        keyConvidado = ndb.Key(urlsafe=idC)
        conv = keyConvidado.get()
        reuniao = conv.reuniao.get()
        owner = reuniao.usr.get()
        tipo = "form"
        values={"conv":conv,
                "reuniao":reuniao,
                "owner":owner,
                "idC":idC,
                "tipo":tipo}        
        self.write_template("resposta.html",values)
        
    def salvar(self,idC):        
        keyConvidado = ndb.Key(urlsafe=idC)
        c = keyConvidado.get()
        c.situacao = int(self.get("situacao"))
        c.resposta = self.get("justificativa")
        c.put()             
        self.write_template("resposta.html")