'''
Created on 02/02/2011

@author: Renzo Nuccitelli
'''
from __future__ import unicode_literals
from common import tmpl
from zen.ce import cengine
import webapp2
from google.appengine.api import users
from usuario.model import User


class BaseHandler(webapp2.RequestHandler):
    def get(self):
        self.make_convetion()
        
    def post(self):
        self.make_convetion()
        
    def make_convetion(self):
        (handler_class, method_name, params) = cengine.to_handler(self.request.path)
        handler = handler_class()
        handler.request = self.request
        handler.get=self.request.get
        handler.write=self.response.out.write
        handler.response = self.response
        handler.handler = self
        handler.redirect=self.redirect
        handler.render=tmpl.render
        def write_template(template_name,values={}):
            user=User.current_user()
            url=None
            logout=None            
            if user:
                logout=users.create_logout_url("/")
            else:
                url=users.create_login_url("/usuario")

            values["current_user"]=user
            values["logout_url"]=logout
            values["login_url"]=url
            
            handler.write(tmpl.render(template_name, values))
            
        handler.write_template=write_template
        method = getattr(handler, method_name)
        method(*params)


app = webapp2.WSGIApplication([("/.*", BaseHandler)], debug = False)

