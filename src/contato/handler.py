'''
Created on 31/08/2012

@author: Daniel
'''
from __future__ import unicode_literals
from google.appengine.api import mail
from zen.ce import cengine

class home():
    def index(self):
        envia_url=cengine.to_path(home.envia)
        values={"envia_url":envia_url}
        self.write_template("contato.html",values)
        
    def envia(self):
        mail.send_mail(sender=self.get("nome")+" <"+self.get("email")+">",
              to="Daniel Cesario <danielatcesario@gmail.com>",
              subject="MinhaReuniao CONTATO",
              body=self.get("mensagem"))
        self.redirect(home.index)      