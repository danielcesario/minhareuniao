from google.appengine.api import users
from google.appengine.ext import ndb
from usuario.model import User
from zen.ce import cengine


class home():
    def index(self):                
        cont_email=User.query(User.email == users.get_current_user().email()).count()
        if cont_email == 0:
            url =cengine.to_path(home.salvar)
            values={"salvar_url":url}
            self.write_template("user_form.html",values)
        else:
            self.redirect("/reuniao")        
        
    def salvar(self):
        google_user=users.get_current_user()
        idUsr=google_user.user_id()
        u=User(id=idUsr,
               name=self.get("nome"),
               empresa=self.get("empresa"),
               funcao=self.get("funcao"),
               email=users.get_current_user().email())
        u.put()
        self.redirect("/reuniao")
        
    def meusdados(self):
        google_user=users.get_current_user()
        key = ndb.Key(User, google_user.user_id())
        usuario = key.get()
        atualizar_url=cengine.to_path(home.salvar)
        values={"usuario":usuario,
                "atualizar_url":atualizar_url}
        self.write_template("user_editar.html",values)
        
    