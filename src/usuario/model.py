from google.appengine.ext import ndb
from google.appengine.api import users

class User(ndb.Model):
    name        =   ndb.StringProperty(required=True)    
    email       =   ndb.StringProperty(required=True)
    empresa     =   ndb.StringProperty(required=True)
    funcao      =   ndb.StringProperty(required=True)
    
    @classmethod
    def current_user(cls):
        google_user=users.get_current_user()
        if google_user:
            return User.get_by_id(google_user.user_id())